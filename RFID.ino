    #include <Servo.h>
#include <LiquidCrystal.h>
#include <SPI.h>
#include <MFRC522.h>

Servo myservo;
int j=0;

LiquidCrystal lcd(A0, A1, A2, A3, A4, A5);

constexpr uint8_t RST_PIN = 9;     // Configurable, see typical pin layout above
constexpr uint8_t SS_PIN = 10;     // Configurable, see typical pin layout above

MFRC522 rfid(SS_PIN, RST_PIN); // Instance of the class

MFRC522::MIFARE_Key key;

// Init array that will store new NUID
byte nuidPICC[4] = {0, 0, 0, 0};
byte i;
/*
  PRONAY MODAK
  AFTAB UDDIN SIFAT
  ZAIDUR RAHMAN
  RAISE UDDIN CHOWDHURY
  MOHAMMAD ALI SABUJ
  MD. ARIFUL ALAM
  PALASH SAHA
  SHATIL MAHMUD AKANDA JOY
  MD. IDRIS ALI

*/
byte z1[4] = {100, 118, 28, 186};
byte z2[4] = {85, 202, 54, 187};
byte z3[4] = {106, 106, 55, 187};
byte z4[4] = {83, 6, 56, 187};
byte z5[4] = {163, 241, 55, 187};
byte z6[4] = {121, 9, 55, 187};
byte z7[4] = {118, 149, 55, 187};
byte z8[4] = {227, 168, 55, 187};
byte z9[4] = {77, 193, 55, 187};


void setup() {
  lcd.begin(16, 2);
  
  SPI.begin(); // Init SPI bus
  rfid.PCD_Init(); // Init MFRC522

  for (i = 0; i < 6; i++) {
    key.keyByte[i] = 0xFF;
  }
  myservo.attach(6);
  myservo.write(0);

  pinMode(0,OUTPUT);
  pinMode(2,OUTPUT);
  digitalWrite(0,LOW);
  digitalWrite(2,HIGH);
}

void loop() {
  get_id();
  matching();
  lcd.setCursor(1, 0);
  lcd.print("PLEASE ENTER A");
  lcd.setCursor(6, 1);
  lcd.print("CARD");
}

void matching()
{
  int a = 1;
  while (a == 1)
  {
    for (i = 0; i < 4; i++)
    {

      if (nuidPICC[i] != 0)
      {
        break;
      }

    }
    if (i == 4) a = 0;
    else a = 2;
  }

  while (a == 2)
  {
    for (i = 0; i < 4; i++)
    {
      if (nuidPICC[i] != z1[i])
      {
        a = 3;
        break;
      }
    }
    if (i == 4)
    {
      a = 0;
      do_1();
      lcd.clear();
      lcd.setCursor(4, 0);
      lcd.print("WELCOME");
      lcd.setCursor(0, 1);
      lcd.print(" PRONAY MODAK!");
      do_2();
      lcd.clear();
    }
  }
  while (a == 3)
  {
    for (i = 0; i < 4; i++)
    {
      if (nuidPICC[i] != z2[i])
      {
        a = 4;
        break;
      }
    }
    if (i == 4)
    {
      a = 0;
      do_1();
      lcd.clear();
      lcd.setCursor(4, 0);
      lcd.print("WELCOME");
      lcd.setCursor(0, 1);
      lcd.print("  AFTAB UDDIN!");
      do_2();
      lcd.clear();
    }
  }
  while (a == 4)
  {
    for (i = 0; i < 4; i++)
    {
      if (nuidPICC[i] != z3[i])
      {
        a = 5;
        break;
      }
    }
    if (i == 4)
    {
      a = 0;
      do_1();
      lcd.clear();
      lcd.setCursor(4, 0);
      lcd.print("WELCOME");
      lcd.setCursor(0, 1);
      lcd.print(" ZAIDUR RAHMAN!");
      do_2();
      lcd.clear();
    }
  }
  while (a == 5)
  {
    for (i = 0; i < 4; i++)
    {
      if (nuidPICC[i] != z4[i])
      {
        a = 6;
        break;
      }
    }
    if (i == 4)
    {
      a = 0;
      do_1();
      lcd.clear();
      lcd.setCursor(4, 0);
      lcd.print("WELCOME");
      lcd.setCursor(0, 1);
      lcd.print("  RAISE UDDIN!");
      do_2();
      lcd.clear();
    }
  }
  while (a == 6)
  {
    for (i = 0; i < 4; i++)
    {
      if (nuidPICC[i] != z5[i])
      {
        a = 7;
        break;
      }
    }
    if (i == 4)
    {
      a = 0;
      do_1();
      lcd.clear();
      lcd.setCursor(4, 0);
      lcd.print("WELCOME");
      lcd.setCursor(0, 1);
      lcd.print(" MOHAMMAD ALI!");
      do_2();
      lcd.clear();
    }
  }
  while (a == 7)
  {
    for (i = 0; i < 4; i++)
    {
      if (nuidPICC[i] != z6[i])
      {
        a = 8;
        break;
      }
    }
    if (i == 4)
    {
      a = 0;
      do_1();
      lcd.clear();
      lcd.setCursor(4, 0);
      lcd.print("WELCOME");
      lcd.setCursor(0, 1);
      lcd.print("   MD. ARIFUL!");
      do_2();
      lcd.clear();
    }
  }
  while (a == 8)
  {
    for (i = 0; i < 4; i++)
    {
      if (nuidPICC[i] != z7[i])
      {
        a = 9;
        break;
      }
    }
    if (i == 4)
    {
      a = 0;
      do_1();
      lcd.clear();
      lcd.setCursor(4, 0);
      lcd.print("WELCOME");
      lcd.setCursor(0, 1);
      lcd.print("  PALASH SAHA!");
      do_2();
      lcd.clear();
    }
  }
  while (a == 9)
  {
    for (i = 0; i < 4; i++)
    {
      if (nuidPICC[i] != z8[i])
      {
        a = 10;
        break;
      }
    }
    if (i == 4)
    {
      a = 0;
      do_1();
      lcd.clear();
      lcd.setCursor(4, 0);
      lcd.print("WELCOME");
      lcd.setCursor(0, 1);
      lcd.print(" SHATIL MAHMUD!");
      do_2();
      lcd.clear();
    }
  }
  while (a == 10)
  {
    for (i = 0; i < 4; i++)
    {
      if (nuidPICC[i] != z9[i])
      {
        a = 11;
        break;
      }
    }
    if (i == 4)
    {
      a = 0;
      do_1();
      lcd.clear();
      lcd.setCursor(4, 0);
      lcd.print("WELCOME");
      lcd.setCursor(0, 1);
      lcd.print(" MD.IDRIS ALI!");
      do_2();
      lcd.clear();
    }
  }


  if (a > 10)
  {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("  WRONG CARD!");
    lcd.setCursor(0, 1);
    lcd.print(" ACCESS DENIED!");
    for(j=1;j<=20;j++)
    {
     if(j%2!=0) digitalWrite(2,LOW);
     else digitalWrite(2,HIGH); 
     digitalWrite(0,HIGH);
     delay(30);
     digitalWrite(0,LOW);
     delay(30); 
    }
    lcd.clear();
  }

  for (i = 0; i < 4; i++) nuidPICC[i] = 0;



}

void get_id()
{

  // Look for new cards
  if ( ! rfid.PICC_IsNewCardPresent())
    return;

  // Verify if the NUID has been readed
  if ( ! rfid.PICC_ReadCardSerial())
    return;


  for (byte i = 0; i < 4; i++) {
    nuidPICC[i] = rfid.uid.uidByte[i];
  }



 

  // Halt PICC
  rfid.PICC_HaltA();

  // Stop encryption on PCD
  rfid.PCD_StopCrypto1();
}
void do_1()
{
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("ACCESS  GRANTED!");
  myservo.write(90);
  for(j=1;j<=10;j++)
  {
    if((j<5)||(j>10&&j<16)) digitalWrite(2,LOW);
    else digitalWrite(2,HIGH);  
    digitalWrite(0,HIGH);
    delay(50);
    digitalWrite(0,LOW);
    delay(50);
  }
}

void do_2()
{
  
  for(j=1;j<=20;j++)
  {
    if((j<5)||(j>10&&j<16)) digitalWrite(2,LOW);
    else digitalWrite(2,HIGH);  
    digitalWrite(0,HIGH);
    delay(50);
    digitalWrite(0,LOW);
    delay(50);
  }
  myservo.write(0);
}


